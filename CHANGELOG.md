# Changelog
Todos los cambios notables del proyecto se documentarán en este archivo.

El formato utilizado está basado en el proyecto [Keep a Changelog](https://keepachangelog.com/es-ES/1.0.0/). 

Para utilizarlo y añadir nuevas entradas, ten en cuenta lo siguiente:

1. Este proyecto se adhiere al [versionado semático](https://semver.org/spec/v2.0.0.html).
2. Las entradas irán en castellano. Esta regla no se aplica a las palabras **Unrelease**, **Added**, **Changed**, **Deprecated**, **Removed**, **Fixed** y **Security**. Éstas se mantienen en inglés para mantener el formato propuesto por *Keep a changelog*.
3. Las entradas deben describir los cambios desde el punto de vista funcional, no técnico. Recuerda que el objeto de este registro es que cualquiera pueda entender los cambios realizados entre versiones.
4. El formato de fechas utilizado es el [ISO 8601](https://www.iso.org/iso-8601-date-and-time-format.html).

## [Unreleased]

### Added
- Se añade la licencia del proyecto.