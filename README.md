# Starti

*Starti* significa *comenzar* en esperanto. Este proyecto va de eso. De comenzar con buen pie.

Un buen programa necesita de una buena infraestructura para empezar. *Starti* te ayudará con esto.

## Qué es

*Starti* define un modelo que puedes copiar para comenzar un nuevo proyecto. Incluye lo básico para garantizar que comienzas con calidad.

## Cómo se usa

1. Realiza un [fork](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html) del proyecto.

## Cambios

Puedes revisar la lista completa de cambios en el [changelog](https://bitbucket.org/danuser2017/starti/src/master/CHANGELOG.md) del proyecto.

## Reconocimientos

### [Logo](https://icon-icons.com/es/icono/avatar-remolones-pereza-perezoso/113274) de *starti*. 

Creado por [Laura Reen](http://laurareen.com/). Utilizado bajo licencia [Creative Commons Atribución 4.0 Internacional (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.es). No se han realizado cambios sobre el mismo. 


## Licencia

The MIT license ([MIT](https://choosealicense.com/licenses/mit/))

Copyright (c) 2019 David Núñez Serrano

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
